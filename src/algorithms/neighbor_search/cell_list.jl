mutable struct CellList{C,S}
    cells::C
    stencil::S
end

abstract type AbstractStencil end

struct Stencil<:AbstractStencil
    rx::UnitRange{Int8}
    ry::UnitRange{Int8}
    rz::UnitRange{Int8}
    cover::Vector{NTuple{3,Int8}}
end

function CellList{C,S}() where {C<:IndexListArray,S<:Stencil}
    return CellList(
        IndexListArray(; dims=(0, 0, 0), nitems=0), S()
    )
end

function CellList(system::MDState, rneigh::Real)
    if !isdiag(system.box_vectors)
        throw(ArgumentError("Non-orthogonal cells are not supported"))
    end
    coords = system.coord
    desired_n_per_cell = BEST_NATOMS_PER_CELL[]

    boxsize = diag(system.box_vectors)
    rdoub = 2 * rneigh
    for ldim in boxsize
        if rdoub > ldim
            throw(ArgumentError("Neighbor radius $rneigh exceeds half of box size $ldim"))
        end
    end

    density = natoms(system) / prod(boxsize)
    best_cell_side = cbrt(desired_n_per_cell / density)
    celldims = floor.(Int, boxsize ./ min(best_cell_side, rneigh)) |> Tuple
    cell_size = boxsize ./ celldims
    stwidth = ceil(Int, rneigh / minimum(cell_size))
    rx, ry, rz = @. ifelse(
        celldims > 2 * stwidth,
        range(-stwidth, stwidth),
        range(-celldims ÷ 2 + iseven(celldims), celldims ÷ 2)
    )
    stencil = Stencil(rx, ry, rz)
    cell_list = CellList(
        IndexListArray(; dims=celldims, nitems=natoms(system)), stencil,
    )
    build_list!(cell_list, celldims, rneigh, coords, boxsize)
    return cell_list
end

function Stencil()
    return Stencil(0:0, 0:0, 0:0, NTuple{3,Int8}[])
end

function Stencil(
    rx::AbstractUnitRange,
    ry::AbstractUnitRange,
    rz::AbstractUnitRange,
)
    return Stencil(rx, ry, rz, similar(1:prod(length, (rx, ry, rz)), NTuple{3,Int8}))
end

#=
Array interface for cell linked lists
=#
Base.isempty(cell_list::CellList) = isempty(cell_list.cells)

Base.@propagate_inbounds function Base.getindex(
    cell_list::CellList, x, y, z
)
    return cell_list.cells[x, y, z]
end

Base.@propagate_inbounds function Base.getindex(
    cell_list::CellList, cartind::CartesianIndex
    )
    return cell_list.cells[cartind]
end

Base.size(cell_list::CellList) = size(cell_list.cells)

@inline function Base.axes(cell_list::CellList, args...)
    axes(cell_list.cells, args...)
end

Base.CartesianIndices(A::CellList) = CartesianIndices(A.cells)

"""
    build_list!(
        cell_list::CellList, r_neigh::Real, coords_list::Vector, box_bounds
    )

Fill `cell_list.cells` with numbers of atoms belonging to the cells, without ordering.
"""
function build_list!(
    cell_list::CellList, list_dims, r_neigh::Real, coords_list::Vector, box_bounds
)
    bounds = box_bounds[1], box_bounds[2], box_bounds[3]

    update_cells!(cell_list, list_dims, coords_list)

    cell_size = bounds ./ list_dims

    fill_stencil!(cell_list.stencil, r_neigh, cell_size)

    fill_cells!(cell_list.cells, coords_list, cell_size, bounds)

    return cell_list
end

function fill_stencil!(
    stencil::Stencil, r_neigh, cell_size
)
    lx, ly, lz = cell_size
    rx, ry, rz = stencil.rx, stencil.ry, stencil.rz
    rsq = r_neigh^2

    ind_dist(i) = iszero(i) ? i : abs(i) - one(i)

    lstencil = 0
    for ind in CartesianIndices((rx, ry, rz))
        i, j, k = Tuple(ind)
        dminsq = mag2((ind_dist(i)*lx, ind_dist(j)*ly, ind_dist(k)*lz))
        if rsq > dminsq
            lstencil += 1
            stencil.cover[lstencil] = (i, j, k)
        end
    end

    resize!(stencil.cover, lstencil)
    return stencil
end

function fill_cells!(cells::IndexListArray, coords, cell_size, bounds)
    for (i, point) in Iterators.reverse(enumerate(coords))
        coord = point[1], point[2], point[3]
        cell_inds = Int.(mod.(coord, bounds) .÷ cell_size)

        push!(cells[cell_inds...], i)
    end
end

"""
    update_cells!(cell_list, list_dims, coords_list)

Re-initialize the dimensions of `cell_list` to `list_dims` and the contents of cells to
    empty sequences.
"""
function update_cells!(cell_list::CellList{<:IndexListArray}, list_dims, coords_list)
    cells = cell_list.cells
    resize!(cells, list_dims; item_capacity=length(coords_list))
    return cell_list
end
