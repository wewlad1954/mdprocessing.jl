function file_list_by_mask(fmask, timesteps)
    nrmpath = fmask |> expanduser |> abspath
    directory = dirname(nrmpath)
    file_list = String[]
    traj_steps = Int[]
    if !ispath(directory) || !isdir(realpath(directory))
        return (; file_list, timesteps=traj_steps)
    end
    path_parts = split(basename(nrmpath), '*')
    prefix_size = sizeof(path_parts[1])
    suffix_size = sizeof(path_parts[2])
    for fname in readdir(directory)
        if startswith(fname, path_parts[1]) && endswith(fname, path_parts[2])
            num_str = @view fname[prefix_size+1:end-suffix_size]
            num = tryparse(Int, num_str)
            if num !== nothing && num in timesteps
                full_path = joinpath(directory, fname)
                rpath = realpath(full_path)
                if isfile(rpath)
                    push!(file_list, full_path)
                    push!(traj_steps, num)
                end
            end
        end
    end

    perm = sortperm(traj_steps)
    file_list = file_list[perm]
    traj_steps = traj_steps[perm]

    return (; file_list, timesteps=traj_steps)
end

"""
    traj_average(fn, fmask::AbstractString; timesteps=0:typemax(Int))

Average a property returned by `fn(state)` over all states written in files corresponding
    to mask `fmask` at timesteps `timesteps`.
"""
function traj_average(
    fn, fmask::AbstractString,
    ;
    timesteps::AbstractVector=0:typemax(Int),
)
    local system
    n_dumps = 0
    local accum
    nrmpath = fmask |> expanduser |> abspath
    directory = dirname(nrmpath)
    if !contains(basename(nrmpath), '*') && ispath(nrmpath)
        rpath = realpath(nrmpath)
        file_list = isfile(rpath) ? [rpath] : String[]
    else
        file_list = file_list_by_mask(fmask, timesteps).file_list
    end

    for fname in file_list
        try
            if n_dumps == 0
                system = read_dump(fname)
            else
                read_dump!(fname, system)
            end
        catch ex
            if ex isa InterruptException
                rethrow(ex)
            else
                @warn """
                    Reading $(fname) failed with error
                    $(sprint(showerror, ex))
                """
                continue
            end
        end
        if n_dumps == 0
            accum = copy(fn(system))
        elseif isimmutable(accum)
            accum = accum .+ fn(system)
        else
            accum .+= fn(system)
        end
        n_dumps += 1
    end
    if n_dumps == 0
        throw(
            ArgumentError(
                "Could not read any file matching mask $(basename(nrmpath)) in $(directory)/"
            )
        )
    end
    return accum / n_dumps
end

"""
    traj_correlate(
        corr_func::Function, fmask::AbstractString
        ;
        step_delta::Integer, initial_steps::AbstractVector)

Correlate a property computed by `corr_func(state, reference)` over an MD trajectory. The
    trajectory is written in files stored in `dir` corresponding to mask `fmask`. The
    correlation between states separated by `dt` is computed (the separation is inferred
    from the file names).
"""
function traj_correlate(
    corr_func::Function, fmask::AbstractString,
    ;
    step_delta::Integer, initial_steps::AbstractVector{<:Integer}=0:typemax(Int)
)
    local system
    local systemref
    local accum
    if !contains(fmask, '*')
        throw(
            ArgumentError(
                "File mask for `traj_correlate` must contain asterisk ('*'), got $fmask"
            )
        )
    end

    nrmpath = fmask |> expanduser |> abspath
    directory = dirname(nrmpath)
    n_dumps = 0

    file_list, traj_steps = file_list_by_mask(fmask, initial_steps)

    for (fname, traj_step) in zip(file_list, traj_steps)
        if traj_step in initial_steps
            shifted_step = traj_step + step_delta
            nshift = searchsorted(traj_steps, shifted_step)
            if !isempty(nshift)
                ishift = first(nshift)
                refname = joinpath(directory, fname)
                compname = joinpath(directory, file_list[ishift])
                try
                    systemref =
                        n_dumps == 0 ?
                        read_dump(refname) :
                        read_dump!(refname, systemref)
                catch ex
                    if ex isa InterruptException
                        rethrow(ex)
                    else
                        @warn """
                            Reading $(refname) failed with error
                            $(sprint(showerror, ex))
                        """
                        continue
                    end
                end
                try
                    system =
                        n_dumps == 0 ?
                        read_dump(compname) :
                        read_dump!(compname, system)
                catch ex
                    if ex isa InterruptException
                        rethrow(ex)
                    else
                        @warn """
                            Reading $(compname) failed with error
                            $(sprint(showerror, ex))
                        """
                        continue
                    end
                end
                corr = corr_func(systemref, system)
                if n_dumps == 0
                    accum = copy(corr)
                elseif isimmutable(accum)
                    accum = accum .+ corr
                else
                    accum .+= corr
                end
                n_dumps += 1
            end
        end
    end
    if n_dumps == 0
        throw(
            ArgumentError(
                "Could not read file pairs matching mask $(basename(nrmpath)) in $(directory)/"
            )
        )
    end
    return accum / n_dumps
end
