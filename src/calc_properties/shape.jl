"""
    gyration_tensor(mol::Molecule)

Return the gyration tensor for a given molecule. If the molecule center is computed as the
    center of mass, then the gyration tensor computation takes into account atom masses.
    For molecules with geometry center, the tensor is computed using only the atomic
    positions.
"""
function gyration_tensor end

function gyration_tensor(mol::Molecule{:mass})
    state = mol.state
    rs = coord_offsets(mol)
    ms = masses(state)
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    for (i, r) in zip(atom_indices(mol), rs)
        m = ms[i]
        rxx += m * r[1] * r[1]
        ryy += m * r[2] * r[2]
        rzz += m * r[3] * r[3]
        rxy += m * r[1] * r[2]
        rxz += m * r[1] * r[3]
        ryz += m * r[2] * r[3]
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz)

    vals, vectors = eigen(gtens)
    vals /= mol.mass
    if det(vectors) < 0
        vectors = -vectors
    end

    return Eigen(vals, vectors)
end

function gyration_tensor(mol::Molecule{:geometry})
    rs = coord_offsets(mol)
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    natoms = 0
    for r in rs
        rxx += r[1] * r[1]
        ryy += r[2] * r[2]
        rzz += r[3] * r[3]
        rxy += r[1] * r[2]
        rxz += r[1] * r[3]
        ryz += r[2] * r[3]
        natoms += 1
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz) / natoms

    vals, vectors = eigen(gtens)
    if det(vectors) < 0
        vectors = -vectors
    end
    return Eigen(vals, vectors)
end

"""
    gyration_radius(mol::Molecule)

Return the gyration radius for a given molecule. If the molecule center is computed as the
    center of mass, then the gyration radius takes into account atom masses. For molecules
    with geometry center, the tensor is computed using only the atomic positions.
"""
function gyration_radius end

function gyration_radius(mol::Molecule{:geometry})
    rs = coord_offsets(mol)
    rxx = ryy = rzz = 0.0
    natoms = 0
    for r in rs
        rxx += r[1] * r[1]
        ryy += r[2] * r[2]
        rzz += r[3] * r[3]
        natoms += 1
    end
    return sqrt((rxx + ryy + rzz) / natoms)
end

function gyration_radius(mol::Molecule{:mass})
    state = mol.state
    rs = coord_offsets(mol)
    ms = masses(state)
    rxx = ryy = rzz = 0.0
    for (i, r) in zip(atom_indices(mol), rs)
        m = ms[i]
        rxx += m * r[1] * r[1]
        ryy += m * r[2] * r[2]
        rzz += m * r[3] * r[3]
    end
    return sqrt((rxx + ryy + rzz) / mol.mass)
end

"""
    asphericity(mol::Molecule)

Compute the asphericity factor
    ``b = \\lambda_z^2 - \\frac{1}{2}(\\lambda_x^2 + \\lambda_y^2)`` where
    ``\\lambda_x^2 \\le \\lambda_y^2 \\le \\lambda_z^2`` are the gyration tensor eigenvalues.
"""
function asphericity(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    return gvals[3] - (gvals[1] + gvals[2]) * 0.5
end

"""
    acilidricity(mol::Molecule)

Compute the acilindricity factor
    ``c = \\lambda_y^2 - \\lambda_x^2`` where
    ``\\lambda_x^2 \\le \\lambda_y^2 \\le \\lambda_z^2`` are the gyration tensor eigenvalues.
"""
function acilindricity(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    return gvals[2] - gvals[1]
end

"""
    shape_anisotropy(mol::Molecule)

Compute the shape anisotropy factor
    ``\\kappa^2 = \\frac{3}{2}\\frac{\\lambda_x^4 + \\lambda_y^4 + \\lambda_z^4}{\\left(
    \\lambda_x^2 + \\lambda_y^2 + \\lambda_z^2 \\right)^2} - \\frac{1}{2}`` where
    ``\\lambda_x^2 \\le \\lambda_y^2 \\le \\lambda_z^2`` are the gyration tensor eigenvalues.
"""
function shape_anisotropy(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    return 1.5 * sum(x->x^2, gvals) / sum(gvals)^2 - 0.5
end

"""
    anisotropy_factors(mol::Molecule)

Return the tuple of asphericity, acilindricity and shape anisotropy factors.
"""
function anisotropy_factors(mol::Molecule)
    gtens = gyration_tensor(mol)
    gvals = gtens.values
    b = gvals[3] - (gvals[1] + gvals[2]) * 0.5
    c = gvals[2] - gvals[1]
    ksq = 1.5 * sum(x->x^2, gvals) / sum(gvals)^2 - 0.5
    return (asphericity=b, acilindricity=c, shape_anisotropy=ksq)
end

"""
    inertia_tensor(mol::Molecule{:mass})

Return the inertia tensor for a given molecule.
"""
function inertia_tensor(mol::Molecule{:mass})
    state = mol.state
    rs = coord_offsets(mol)
    ms = masses(state)
    Ixx = Iyy = Izz = Ixy = Ixz = Iyz = 0.0
    for (i, r) in zip(atom_indices(mol), rs)
        m = ms[i]
        rxx, ryy, rzz = r.^2
        Ixx += m * (ryy + rzz)
        Iyy += m * (rxx + rzz)
        Izz += m * (rxx + ryy)
        Ixy += m * r[1] * r[2]
        Ixz += m * r[1] * r[3]
        Iyz += m * r[2] * r[3]
    end
    gtens = SMatrix{3,3}(Ixx, -Ixy, -Ixz, -Ixy, Iyy, -Iyz, -Ixz, -Iyz, Izz)

    vals, vectors = eigen(gtens)
    if det(vectors) < 0
        vectors = -vectors
    end
    return Eigen(vals, vectors)
end
