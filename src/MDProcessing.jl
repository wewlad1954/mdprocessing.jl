module MDProcessing

using Base.Sort
using LinearAlgebra
using MappedArrays
using OffsetArrays
using Printf: @format_str, Format, format
using StaticArrays
using Statistics

export MDState
export read_dump, read_dump!
export addproperty!, update_coord!, clear_state!

export hasmass, masses, wrapped_positions, unwrapped_positions, velocities
export natoms, boxvectors, boxorigin
export id_tags, mol_tags
export wrapped_position, unwrapped_position, velocity

export molecules
export coord_offsets, atom_indices, centering
export id_tag, mol_tag

export neighbor_pairs, pbcvector, pbcdist, pbcdist2

export gyration_radius, gyration_tensor, inertia_tensor
export asphericity, acilindricity, shape_anisotropy, anisotropy_factors
export rdf, rdf!, rdfw, rdfw!, kin_energy, msd, vel_distribution, vel_distribution!
export molecule_planes, molecule_lines
export msdt
export traj_average, traj_correlate
export Autocorr

const BEST_NATOMS_PER_CELL = Ref(16)

include("types/linked_list.jl")
include("types/md_state.jl")
include("types/molecules.jl")
include("algorithms/neighbor_search/cell_list.jl")
include("algorithms/neighbor_search/neigh_search.jl")
include("types/particle_pair.jl")
include("types/neigh_pairs.jl")
include("iteration/md_state.jl")
include("interface/mdstate.jl")
include("interface/molecules.jl")
include("calc_properties/shape.jl")
include("math.jl")
include("md_statistics.jl")
include("io_utils.jl")
include("trajectory.jl")
include("autocorr/autocorr.jl")

end # module MDProcessing
