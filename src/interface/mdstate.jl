function clear_state!(system::MDState)
    empty!(system.coord)
    empty!(system.vel)
    empty!(system.id)
    empty!(system.mol)
    empty!(system.type)
    empty!(system.image)
    empty!(system._scalars)
    empty!(system._vectors)
    system.nreal = 0
    return system
end

"""
    natoms(system::MDState)

Return the number of atoms in the system.
"""
natoms(system) = system.nreal # length(system.coord)

"""
    boxvectors(system::MDState)

Return the 3x3 matrix of box basis vectors.
"""
boxvectors(system::MDState) = system.box_vectors

"""
    boxorigin(system::MDState)

Return the vector of the box coordinate origin.
"""
boxorigin(system::MDState) = system.origin

"""
    wrapped_positions(system::MDState)

Return an array of particle coordinates in `system` wrapped into the simulation box.
"""
function wrapped_positions(system::MDState)
    return system.coord
end

"""
    unwrapped_positions(system::MDState)

Return an array of unwrapped particle coordinates in `system`.
"""
function unwrapped_positions(system::MDState)
    boxvecs = boxvectors(system)
    coord = system.coord
    image = system.image
    return mappedarray((r, img) -> r + boxvecs * img, coord, image)
end

"""
    velocities(system::MDState)

Return an array of particle velocities in `system`.
"""
function velocities(system::MDState)
    return system.vel
end

"""
    hasmass(system::MDState)

Return `true` if `system` contains masses information.
"""
function hasmass(system::MDState)
    return haskey(system._scalars, :mass)
end

"""
    masses(system::MDState)

Return the vector of masses in `system`. If masses are not present, then `missing` is
    returned.
"""
function masses(system::MDState)
    return get(system._scalars, :mass, missing)
end

"""
    id_tags(system::MDState)

Return atomic IDs for atoms in `system`.
"""
id_tags(system::MDState) = system.id

"""
    mol_tags(system::MDState)

Return molecule IDs for atoms in `system`.
"""
mol_tags(system::MDState) = system.mol

function wrapped_position(p::MDParticle)
    return wrapped_positions(p.state)[p.idx]
end

function unwrapped_position(p::MDParticle)
    return unwrapped_positions(p.state)[p.idx]
end

function velocity(p::MDParticle)
    return velocities(p.state)[p.idx]
end

function id_tag(p::MDParticle)
    return id_tags(p.state)[p.idx]
end

function mol_tag(p::MDParticle)
    return mol_tags(p.state)[p.idx]
end
