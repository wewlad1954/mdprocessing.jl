"""
    centering(::Molecules)

Return `:geometry` if the geometric molecule center is used or `:mass` if center of mass is
    used.
"""
centering(::Molecules{C}) where {C} = C

"""
    centering(::Molecule)

Return `:geometry` if the geometric molecule center is used or `:mass` if center of mass is
    used.
"""
centering(::Molecule{C}) where {C} = C

"""
    atom_indices(mol::Molecule)

Return an iterable with indices of atoms belonging to molecule `mol`.
"""
atom_indices(mol::Molecule) = mol.atom_inds

"""
    coord_offsets(mol::Molecule)

Return an iterable of atomic coordinate differences from the molecule center.
"""
Base.@propagate_inbounds function coord_offsets(mol::Molecule)
    state = mol.state
    pbc = state.pbc
    boxsize = diag(boxvectors(state))
    atomcoords = wrapped_positions(state)
    centercoord = wrapped_position(mol)
    return (pbcvector(centercoord, atomcoords[i], boxsize, pbc) for i in atom_indices(mol))
end

"""
    id_tags(mol::Molecules)

Return molecule IDs for molecules in `mol`.
"""
function id_tags(mol::Molecules)
    return mol.id
end

"""
    mol_tags(mol::Molecules)

Return molecule IDs for molecules in `mol`.
"""
function mol_tags(mol::Molecules)
    return mol.id
end

"""
    velocities(mol::Molecules)

Return an iterable of molecular velocities in `mol`.
"""
function velocities(mol::Molecules)
    return (m.vel for m in mol)
end

"""
    atom_masses(mol::Molecule{:mass})

Return an iterable over the masses of atoms in `mol`.
"""
function atom_masses(mol::Molecule{:mass})
    ms = masses(mol.state)
    return (ms[i] for i in atom_indices(mol))
end
