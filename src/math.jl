"""
    clamp(ind, imax)

Return `ind` if it is in the range `0:imax`, or wrap it to that range by adding or
    subtracting `imax`
"""
function clamp(ind, imax)
    if ind < 0
        ind + imax
    elseif ind < imax
        ind
    else
        ind - imax
    end
end

"""
    mag2(vec)

Return the squared magnitude (sum of the squares of the components) of `vec`.
"""
mag2(vec) = sum(x->x^2, vec)

"""
    dot3(a, b, c)

Return the scalar-vector product `dot(a, cross(b, c))` of length-3 vectors.
"""
Base.@propagate_inbounds function dot3(a, b, c)
    @boundscheck if !(length(a) == length(b) == length(c) == 3)
        throw(DimensionMismatch("length-3 vectors expected"))
    end

    @inbounds prod = a[1] * (b[2] * c[3] - b[3] * c[2]) +
        a[2] * (b[3] * c[1] - b[1] * c[3]) +
        a[3] * (b[1] * c[2] - b[2] * c[1])
    return prod
end

kin_energy(velocities) = sum(mag2, velocities) / 2

function kin_energy(system::MDState)
    if hasproperty(system, :mass)
        m = system.mass
        v = system.vel
        return sum(i -> m[i] * v[i]^2, eachindex(m, v)) / 2
    else
        kin_energy(system.vel)
    end
end

"""
    pbcvector(r1, r2, size)

Return the vector `r2 - r1`, assuming they are in an orthogonal box with sides `size`
    under periodic boundary conditions.
"""
function pbcvector(
    r1::StaticVector{N,T}, r2::StaticVector{N,T}, size::StaticVector{N},
) where {N,T}
    Δr = (r2 - r1)
    halfsize = size / 2
    Δr = ifelse.(Δr .>= halfsize, Δr - size, Δr)
    Δr = ifelse.(Δr .< -halfsize, Δr + size, Δr)

    return Δr
end

"""
    pbcvector(r1, r2, size, pbc)

Return the vector `r2 - r1`, assuming they are in an orthogonal box with sides `size`. `pbc`
    specifies if the direction has periodic boundary conditions.
"""
function pbcvector(
    r1::StaticVector{N,T},
    r2::StaticVector{N,T},
    size::StaticVector{N},
    pbc::Union{StaticVector{3,Bool}, NTuple{3,Bool}}
) where {N,T}
    Δr = (r2 - r1)
    halfsize = size / 2
    Δr = ifelse.(pbc, ifelse.(Δr .>= halfsize, Δr - size, Δr), Δr)
    Δr = ifelse.(pbc, ifelse.(Δr .< -halfsize, Δr + size, Δr), Δr)

    return (Δr)
end

function pbcvector(pair::ParticlePair)
    return pair.r12
end

"""
    pbcdist(r1, r2, size)

Compute the distance between `r1` and `r2` in a box with size `size` assuming periodic
    boundary conditions.
"""
function pbcdist(r1::AbstractVector{T}, r2::AbstractVector{T}, size) where {T<:Real}
    @boundscheck if !(length(r1) == length(r2) == length(size) == 3)
        throw(DimensionMismatch("length-3 vectors expected"))
    end
    mag2 = zero(float(T))
    for (r1i, r2i, Li) in zip(r1, r2, size)
        dri = (r2i - r1i) % size
        if dri > Li / 2
            dri -= Li
        elseif dri < -Li / 2
            dri += Li
        end
        mag2 += dri^2
    end
    return sqrt(mag2)
end

function pbcdist(
    r1::StaticVector{N,T}, r2::StaticVector{N,T}, size::StaticVector{N}
) where {N,T<:Real}
    Δr = pbcvector(r1, r2, size)
    return norm(Δr)
end

function pbcdist(pair::ParticlePair)
    return sqrt(pair.sqdist)
end

function pbcdist2(
    r1::StaticVector{N,T}, r2::StaticVector{N,T}, size::StaticVector{N}
) where {N,T<:Real}
    Δr = pbcvector(r1, r2, size)
    return mag2(Δr)
end

function pbcdist2(pair::ParticlePair)
    return pair.sqdist
end

function coss(r1, r2)
    q = mag2(r1)
    w = mag2(r2)
    return (r1 ⋅ r2) / sqrt(q * w)
end

"""
    integrate_trap!(runsum::AbstractVector, input::AbstractVector, dx::Number)

Fill `runsum` with the running integral values of `input`. The integration is performed by
    the trapezoid method, and `input` should contain the values of function at points
    separated by `dx`.
"""
function integrate_trap!(runsum::AbstractVector, input::AbstractVector, dx::Number)
    for n in 2:length(runsum)
        runsum[n] = runsum[n-1] + 0.5 * dx * (input[n-1] + input[n])
    end
    return runsum
end

"""
    integrate_trap(input::AbstractVector, dx::Number)

Return an array of running integral values of `input`. The integration is performed by
    the trapezoid method, and `input` should contain the values of function at points
    separated by `dx`.
"""
function integrate_trap(input::AbstractVector{T}, dx::Number=one(T)) where {T<:Number}
    runsum = similar(input, float(T))
    return integrate_trap!(runsum, input, dx)
end

"""
    normal_calc(coords)

Compute the normal to the plane inscribed by the least squares method in the point cloud.
"""
function normal_calc(coords, p=sum(coords) / length(coords))
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    for r in coords
        rx, ry, rz = r[1] - p[1], r[2] - p[2], r[3] - p[3]
        rxx += rx * rx
        ryy += ry * ry
        rzz += rz * rz
        rxy += rx * ry
        rxz += rx * rz
        ryz += ry * rz
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz) / natoms

    vals, vectors = eigen(gtens)
    nor = vectors[:, 1]
    if length(coords) > 1
        r1, r2 = coords
        sr1 = SVector(r1[1] - p[1], r1[2] - p[2], r1[3] - p[3])
        sr2 = SVector(r2[1] - p[1], r2[2] - p[2], r2[3] - p[3])
        sign = dot3(sr1, sr2, nor)
        if sign < 0
            nor = -nor
        end
    end
    return nor
end

"""
    normal_calc(mol::Molecule)

Compute the normal to the plane inscribed by the least squares method in the point cloud.
"""
function normal_calc(mol::Molecule)
    gtens = gyration_tensor(mol)

    vals, vectors = gtens
    nor = vectors[:, 1]
    if length(mol) > 1
        r1, r2 = coord_offsets(mol)
        sign = dot3(r1, r2, nor)
        if sign < 0
            nor = -nor
        end
    end
    return nor
end

"""
    line_calc(coords, center=sum(coords) / length(coords))

Compute the best-fit straight-line approximation to the point cloud in the sense of total
    least squares.
"""
function line_calc(coords, p=sum(coords) / length(coords))
    rxx = ryy = rzz = rxy = rxz = ryz = 0.0
    for r in coords
        rx, ry, rz = r[1] - p[1], r[2] - p[2], r[3] - p[3]
        rxx += rx * rx
        ryy += ry * ry
        rzz += rz * rz
        rxy += rx * ry
        rxz += rx * rz
        ryz += ry * rz
    end
    gtens = SMatrix{3,3}(rxx, rxy, rxz, rxy, ryy, ryz, rxz, ryz, rzz) / natoms

    vals, vectors = eigen(gtens)
    dir = vectors[:, 3]
    if length(coords) > 1
        imin = imax = 0, 0
        dmin = Inf
        dmax = -Inf
        for (i, r) in coords
            d = dot(r - p, dir)
            if d < dmin
                imin, dmin = i, d
            elseif d > dmax
                imax, dmax = i, d
            end
        end
        if imax < imin
            dir = -dir
        end
    end
    return dir
end

function line_calc(mol::Molecule)
    gtens = gyration_tensor(mol)

    vals, vectors = gtens
    dir = vectors[:, 3]
    if length(mol) > 1
        imin = imax = 0, 0
        dmin = Inf
        dmax = -Inf
        for (i, r) in coord_offsets(mol)
            d = dot(r, dir)
            if d < dmin
                imin, dmin = i, d
            elseif d > dmax
                imax, dmax = i, d
            end
        end
        if imax < imin
            dir = -dir
        end
    end
    return nor
end
