"""
    msd(state, ref_state)

Compute the mean square displacement of particles in `state` relative to `ref_state`.
"""
function msd(state::MDState{T}, ref_state::MDState{T}) where {T}
    if state.id != ref_state.id
        error("Cannot compute MSD: IDs do not coincide")
    end

    dr2 = zero(T)
    coord = unwrapped_positions(state)
    ref_coord = unwrapped_positions(ref_state)
    scale = inv(natoms(ref_state))
    for idx in eachindex(coord, ref_coord)
        Δr = coord[idx] - ref_coord[idx]
        dr2 += mag2(Δr) * scale
    end
    return dr2
end

"""
    vel_distribution!(dist, system, vmin, vmax, dv)

Compute distribution of velocity components in the range from `vmin` to `vmax`
with the step `dv` and overwrite `dist` with it.
"""
function vel_distribution!(
    dist::AbstractVector, system::MDState{T}, vmin, vmax, dv
) where {T}
    vel = system.vel
    dist .= 0
    for iatom in 1:length(vel), i in 1:3
        nbin = floor(Int, (vel[iatom][i] - vmin) / dv) + 1
        if nbin in eachindex(dist)
            dist[nbin] += 1
        end
    end
    dist ./= sum(dist)
    return dist
end

"""
    vel_distribution(system, vmin, vmax, dv)

Compute distribution of velocity components in the range from `vmin` to `vmax`
with the step `dv`.
"""
function vel_distribution(system::MDState, vmin, vmax, dv)
    n_distr_bins = ceil(Int, (vmax - vmin) / dv)
    distr = zeros(Float64, n_distr_bins)
    return vel_distribution!(distr, system, vmin, vmax, dv)
end

# function coordination(system::MDState, rmax::Real)
#     build_cell_list!(system, rmax)
#     coord = system.coord
#     cell_list = system.cell_list
#     success_checks = 0
#     fail_checks = 0
#     boxsize = diag(boxvectors(system))
#     V = prod(boxsize)
#     for i in CartesianIndices(cell_list)
#         isempty(cell_list[i]) && continue
#         neigh_i = neigh_atoms(system, i)
#         for atom1 in neigh_i
#             if atom1 > natoms(system) # system.nreal
#                 break
#             end
#             @inbounds r1 = coord[atom1]
#             for atom2 in cell_list[i]
#                 atom2 < atom1 || break
#                 @inbounds r2 = coord[atom2]
#                 Δr12sq = pbcdist2(r1, r2, boxsize)
#                 if Δr12sq < rmax^2
#                     success_checks += 1
#                 else
#                     fail_checks += 1
#                 end
#             end
#         end
#     end
#     ntotal = natoms(system)
#     return (
#         coordination = success_checks / ntotal,
#         fails = fail_checks / ntotal,
#         particles_per_cell = ntotal / prod(size(system.cell_list))
#     )
# end

"""
    rdf!(gr, system, rmax, nslice)

Compute radial distribution function (RDF) over all atoms in `system` and overwrite `gr`
    with it. `rmax` defines the cutoff radius for the computed RDF, the length or number of
    rows in `gr` defines the number of histogram bins.

# Arguments
- `gr::AbstractVecOrMat`: the array to write the RDF to. If `gr isa AbstractVector`, then
    only g(r) values are written. If `gr isa AbstractMatrix`, then the the coordinates of
    histogram bin centers are written into the first column, the g(r) values to the second
    column
- `system::MDState`: the system for which the RDF is calculated
- `rmax::Real`: radius up to which the RDF is calculated

# Returns
- `AbstractVector`: the array `gr` overwritten by the RDF
"""
function rdf!(gr::AbstractVector, system::MDState, rmax::Real)
    nslice = length(gr)
    dr = rmax / nslice
    invdr = inv(dr)
    boxsize = diag(boxvectors(system))
    V = prod(boxsize)
    gr .= 0

    for pair in NeighborPairs(system, rmax)
        slice_index = floor(Int, pbcdist(pair) * invdr) + 1
        gr[slice_index] += 1
    end
    ntotal = natoms(system)
    density = ntotal / V
    vol_prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = vol_prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

function rdf!(gr::AbstractMatrix, system::MDState, rmax::Real)
    nbins = size(gr, 1)
    dr = rmax / nbins
    rrange = dr/2 : dr : rmax
    @views gr[:, 1] .= rrange
    rdf!(@view(gr[:, 2]), system, rmax)
    return gr
end

"""
    rdf(system, rmax, nslice)

Compute radial distribution function (RDF) over all atoms in `system`.

# Arguments
- `system::MDState`: the system for which the RDF is calculated
- `rmax::Real`: radius up to which the RDF is calculated
- `nslice::Integer`: number of slices in the RDF

# Returns
- `Matrix{Float64}`: the computed RDF. The first column stores the centers of histogram
    bins, the second column stores the RDF values.
"""
function rdf(system::MDState, rmax::Real, nslice::Integer)
    return rdf!(zeros(Float64, nslice, 2), system, rmax)
end

"""
    fourpoint_cc(system, systemnext, rbracket)

Compute the correlation coefficient between displacements of atom pairs in `system` and
    `systemnext` that are within `rbracket` apart in `system`.

# Arguments
- `system::MDState`: the base system
- `systemnext::MDState`: the system with displaced atoms
- `rbracket::Tuple`: the distance bracket in the form `(rmin, rmax)`

# Returns
- `Float64`: the correlation coefficient
"""
# function fourpoint_cc(
#     system::MDState,
#     systemnext::MDState,
#     rbracket::Tuple{Real,Real},
# )
#     rlo, rhi = rbracket
#     build_cell_list!(system, rhi)
#     coord = system.coord
#     coordnext = systemnext.coord
#     dcoord = coordnext .- coord
#     cell_list = system.cell_list
#     boxsize = diag(system.box_vectors)
#     accum = 0.0
#     pair_count = 0
#     for idx in CartesianIndices(cell_list)
#         neigh_i = neigh_atoms(system, idx)
#         for i in neigh_i
#             r1 = coord[i]
#             d1 = dcoord[i]
#             for j in cell_list[idx]
#                 j < i || break
#                 r2 = coord[j]
#                 d2 = dcoord[j]
#                 Δr12 = pbcdist(r1, r2, boxsize)
#                 if rlo < Δr12 < rhi
#                     accum += coss(d1, d2)
#                     pair_count += 1
#                 end
#             end
#         end
#     end
#     return pair_count == 0 ? 0.0 : accum / pair_count
# end


"""
    fourpoint_cc(system, systemnext; r₀, thickness)

Compute the correlation coefficient between displacements of atom pairs in `system` and
    `systemnext` for which the distances are in the interval
    `(r₀ - thickness/2, r₀ + thickness/2)`.

# Arguments
- `system::MDState`: the base system
- `systemnext::MDState`: the system with displaced atoms

# Keywords
- `r₀`: the median radius of the spherical layer
- `thickness`: the thickness of the spherical layer

# Returns
- `Float64`: the correlation coefficient
"""
# function fourpoint_cc(
#     system::MDState,
#     systemnext::MDState;
#     r₀::Real,
#     thickness::Real,
# )
#     rbracket = (r₀ - thickness / 2, r₀ + thickness / 2)
#     return fourpoint_cc(system, systemnext, rbracket)
# end

# function fourpoint_cc_rsweep!(
#     accum::AbstractVector,
#     lengths::AbstractVector,
#     system::MDState,
#     systemnext::MDState;
#     rmax::Real,
#     nbin::Real,
# )
#     build_cell_list!(system, rmax)
#     coord = system.coord
#     coordnext = systemnext.coord
#     cell_list = system.cell_list
#     boxsize = system.size
#     dr = rmax / nbin
#     for idx in CartesianIndices(cell_list)
#         neigh_i = neigh_atoms(system, idx)
#         for i in neigh_i
#             r1 = coord[i]
#             d1 = coordnext[i] - r1
#             for j in cell_list[idx]
#                 j < i || break
#                 r2 = coord[j]
#                 Δr12 = pbcdist(r1, r2, boxsize)
#                 if Δr12 < rmax
#                     d2 = coordnext[j] - r2
#                     slice_index = floor(Int, Δr12 / dr) + 1
#                     accum[slice_index] += coss(d1, d2)
#                     lengths[slice_index] += 1
#                 end
#             end
#         end
#     end
#     map!(accum, accum, lengths) do (acc, n)
#         ave = acc / n
#         return isnan(ave) ? zero(ave) : ave
#     end
#     return accum
# end

# function fourpoint_cc_rsweep(
#     system::MDState{T},
#     systemnext::MDState{T};
#     rmax::Real,
#     nbin::Real,
# ) where {T}
#     accum = similar(system.coord, T)
#     lengths = similar(accum, Int)
#     return fourpoint_cc_rsweep!(accum, lengths, system, systemnext; rmax, nbin)
# end

"""
    mol_com(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for all molecules in `system` and having the center-of-mass
    velocities of molecules.
"""
function mol_com(system::MDState{T}) where T
    return map(_->NamedTuple(), Molecules(system))
end

function c_mass_for_mol(coords, mass)
    cmass = zero(eltype(coords))
    molmass = 0.0
    for idx in 1:length(coords)
        cmass += mass[idx] * coords[idx]
        molmass += mass[idx]
    end
    cmass = cmass / molmass
    return cmass
end

"""
    rdfw!(gr, system, rmax, nslice; weights::AbstractVector)

Compute radial distribution function using `weights` as weights for atom types and
    overwrite `gr` with it.
"""
function rdfw!(
    gr::AbstractVector, system::MDState, rmax::Real;
    weights::AbstractVector
)
    nslice = length(gr)
    dr = rmax / nslice
    boxsize = diag(boxvectors(system))
    ntotal = sum(system) do part
        get(weights, part.type, zero(eltype(weights)))
    end
    V = prod(boxsize)
    gr .= 0
    function exclude(i, j)
        w1 = get(weights, system.type[i], nothing)
        w2 = get(weights, system.type[j], nothing)
        w1 == 0 || w1 === nothing || w2 == 0 || w2 === nothing
    end
    foreach(neighbor_pairs(system, rmax; exclude)) do pair
        atom1, atom2 = atom_indices(pair)
        slice_index = floor(Int, pbcdist(pair) / dr) + 1
        w1 = weights[system.type[atom1]]
        w2 = weights[system.type[atom2]]
        gr[slice_index] += w1 * w2
    end
    density = ntotal / V
    prefactor = 4/3 * π * dr^3
    for i in eachindex(gr)
        vol_i = prefactor * (i^3 - (i-1)^3)
        npair_ref = density * vol_i
        gr[i] /= ntotal * npair_ref / 2
    end
    return gr
end

function rdfw!(gr::AbstractMatrix, system::MDState, rmax::Real; weights)
    nbins = size(gr, 1)
    dr = rmax / nbins
    rrange = dr/2 : dr : rmax
    @views gr[:, 1] .= rrange
    rdfw!(@view(gr[:, 2]), system, rmax; weights)
    return gr
end

"""
    rdfw(system, rmax, nslice; weights)

Compute radial distribution function with weights.
"""
function rdfw(
    system::MDState, rmax::Real, nslice::Integer;
    weights::AbstractVector{<:Real}
)
    return rdfw!(zeros(Float64, nslice, 2), system, rmax; weights)
end

"""
    molecule_planes(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for molecules in `system`. Particles get a vector property
    `:normal` which is the result of `normal_calc` for each molecule.
"""
function molecule_planes(system::MDState)
    mols = molecules(system; centering=:mass)
    buf = map(mols) do mol
        return (; normal=normal_calc(mol))
    end
    return buf
end

"""
    molecule_planes(mols::Molecules)

Return an `MDState` with the same box bounds as `mols`' parent state with particles located
    at the center-of-mass positions for molecules in `system`. Particles get a vector
    property `:normal` which is the result of `normal_calc` for each molecule.
"""
function molecule_planes(mols::Molecules)
    buf = map(mols) do mol
        return (; normal=normal_calc(mol))
    end
    return buf
end

function molecule_planes(dump_file::AbstractString)
    system = read_dump(dump_file)
    return molecule_planes(system)
end

"""
    molecule_lines(system::MDState)

Return an `MDState` with the same box bounds as `system` with particles located at the
    center-of-mass positions for molecules in `system`. Particles get a vector property
    `:direction` which is the result of `line_calc` for each molecule.
"""
function molecule_lines(system::MDState)
    mols = molecules(system; centering=:mass)
    buf = map(mols) do mol
        return (; direction=line_calc(mol))
    end
    return buf
end

function correlate!(buf, vecs0, vecs1)
    buf .= dot.(vecs0, vecs1)
    return buf
end

function corr_normals(system0, system1)
    _, nrm0 = molecule_planes(system0)
    _, nrm1 = molecule_planes(system1)
    return correlate!(similar(molecules(system0), Float64), nrm0, nrm1)
end

"""
    msdt(dir::AbstractString, mask::AbstractString, dt::Integer)

Compute the average molecular MSD for all dumps in `dir` that match `mask` and are `dt` time steps apart.
`mask` has the form `file.*.extension` where `*` is the timestep number
(following the LAMMPS naming convention).
"""
function msdt(direct::AbstractString, mask::AbstractString, dt::Integer)
    return traj_correlate(direct, mask, dt) do system, systemref
        molec_com = mol_com(system)
        molec_com_ref = mol_com(systemref)
        return msd(molec_com, molec_com_ref)
    end
end

function correlate_normals(mask::AbstractString, dt::Integer)
    dot_buf = similar(molecules, Float64)
    function nrm_corr(system, systemref)
        buf = molecule_planes(system)
        buf_ref = molecule_planes(systemref)
        dot_buf .= dot.(buf.normal, buf_ref.normal)
        return mean(dot_buf)
    end
    return traj_correlate(nrm_corr, mask; step_delta=dt)
end
