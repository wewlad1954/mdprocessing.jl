struct IndexList{T<:Integer}
    data::Vector{T}
    index::Int
    items_offset::Int
end

"""
    IndexListArray{N,T<:Integer}<:AbstractArray{N,IndexList{T}}

Data structure to store a 0-based `N`-dimentional array of linked lists.

    # Structure:

    * `data[1]` is `ncells`, the total number of array cells
    * `data[2:N+1]` are the array dimensions
    * `data[N+2:N+ncells+1]` is the `heads` array, containing the numbers of the first item
        in the corresponding cell
    * `data[N+ncells+2]` is the number of items
    * `data[N+ncells+3:end]` are the items data, arranged as a linked list

    # Linked list structure

    Denote `data[N+2:N+ncells+1]` as `heads` and `data[N+ncells+3:end]` as `items`.
    `n = heads[i]` is the index of the first item in the cell `i`. If `n == 0`, then the
    corresponding cell is empty. The next item in that cell is `items[n]`, the next is
    `items[items[n]]` etc. until `items[k] == 0`.
"""
struct IndexListArray{N,T<:Integer}<:AbstractArray{N,IndexList{T}}
    data::Vector{T}

    function IndexListArray{N,T}(
        ;
        dims::NTuple{N,Integer}=(0,0,0), nitems::Integer=0
    ) where {N,T<:Integer}
        ncells = prod(dims)
        data = zeros(T, N + ncells + nitems + 2)
        @inbounds data[1] = ncells
        @inbounds data[2:N+1] .= dims
        @inbounds data[N + ncells + 2] = nitems
        return new{N,T}(data)
    end
end

function IndexListArray(
    ;
    dims::NTuple{N,Integer}=(0,0,0), nitems::Integer=0
) where {N}
    T = promote_type(typeof.(dims)...)
    return IndexListArray{N,T}(; dims, nitems)
end

function Base.isempty(cell_list::IndexListArray{N}) where {N}
    data = cell_list.data
    @inbounds ncells = data[1]
    @inbounds for ind in N+2:N+ncells+1
        data[ind] == 0 || return false
    end
    return true
end

function Base.size(cell_list::IndexListArray{N}) where {N}
    data = cell_list.data
    @inbounds return ntuple(i->data[i+1], N)
end

function Base.length(cell_list::IndexListArray)
    data = cell_list.data
    @inbounds return data[1]
end

@inline function Base.axes(cell_list::IndexListArray)
    dims = size(cell_list)
    return map(d->0:d-1, dims)
end

@inline function Base.axes(cell_list::IndexListArray{N}, d::Integer) where {N}
    d <= N ? axes(cell_list)[d] : 0:0
end

Base.IndexStyle(::Type{<:IndexListArray}) = IndexLinear()

"""
    resize!(list::IndexListArray, dims::Tuple; item_capacity=num_items(list))

Resize `list` to contain cells array with dimensions `dims`. Optionally, resize the
    item storage capacity `item_capacity`. The cells are emptied after the resize.
"""
function Base.resize!(
    list::IndexListArray{N}, dims::NTuple{N,Integer},
    ;
    item_capacity=nitems(list),
) where {N}
    data = list.data
    ncells = prod(dims)
    resize!(data, N + ncells + item_capacity + 2)
    @inbounds data[1] = ncells
    @inbounds data[2:N+1] .= dims
    @inbounds data[N+2:N+ncells+1] .= 0
    @inbounds data[N + ncells + 2] = item_capacity
    return list
end

"""
    heads_offset(list::IndexListArray)

Return the index in `list.data` after which the list heads are stored.
"""
function heads_offset(::IndexListArray{N}) where {N}
    return N + 1
end

"""
    items_offset(list::IndexListArray)

Return the index in `list.data` after which the array of items starts.
"""
function items_offset(cell_list::IndexListArray{N}) where {N}
    data = cell_list.data
    @inbounds ncells = data[1]
    return N + ncells + 2
end

"""
    nitems(list::IndexListArray)

Return the total number of items stored in the linked lists joined in `list`.
"""
function nitems(cell_list::IndexListArray{N}) where {N}
    data = cell_list.data
    @inbounds ncells = data[1]
    @inbounds return data[N + ncells + 2]
end

"""
    cart2lin(inds, dims)

Return the indices `ind` in a zero-based array with dimensions `dims` converted to an index
    in Julia `Vector`.
"""
@inline function cart2lin(inds::NTuple{N,Integer}, dims::NTuple{N,Integer}) where {N}
    @inbounds accum = inds[N]
    @inbounds for ind in N-1:-1:1
        accum = muladd(accum, dims[ind], inds[ind])
    end
    return accum + 1
end

Base.@propagate_inbounds function Base.eachindex(
    ::IndexLinear, cell_list::IndexListArray
)
    ncells = cell_list.data[1]
    return 0:ncells-1
end

Base.@propagate_inbounds function Base.eachindex(
    ::IndexCartesian, cell_list::IndexListArray
)
    return CartesianIndices(cell_list)
end

Base.@propagate_inbounds function Base.getindex(
    cell_list::IndexListArray, ind::Integer
)
    @boundscheck ind in eachindex(cell_list) || throw(BoundsError(cell_list, ind))
    head_ind = ind + 1 + heads_offset(cell_list)
    return IndexList(cell_list.data, head_ind, items_offset(cell_list))
end

Base.@propagate_inbounds function Base.getindex(
    cell_list::IndexListArray{N}, inds::Integer...
) where {N}
    dims = size(cell_list)
    @boundscheck all(in.(inds, axes(cell_list))) || throw(BoundsError(cell_list, inds))
    @inbounds head_ind = cart2lin(inds, dims) + heads_offset(cell_list)
    return IndexList(cell_list.data, head_ind, items_offset(cell_list))
end

Base.@propagate_inbounds function Base.getindex(
    cell_list::IndexListArray{N}, ind::CartesianIndex{N}
) where {N}
    inds = Tuple(ind)
    dims = size(cell_list)
    @boundscheck all(in.(inds, axes(cell_list))) || throw(BoundsError(cell_list, inds))
    @inbounds head_ind = cart2lin(inds, dims) + heads_offset(cell_list)
    return IndexList(cell_list.data, head_ind, items_offset(cell_list))
end

Base.CartesianIndices(A::IndexListArray) = CartesianIndices(axes(A))

Base.@propagate_inbounds function Base.push!(grid_cell::IndexList, i::Integer)
    head_ind = grid_cell.index
    offset = grid_cell.items_offset
    grid_cell.data[offset + i] = grid_cell.data[head_ind]
    grid_cell.data[head_ind] = i
    return grid_cell
end

Base.@propagate_inbounds function Base.iterate(
    grid_cell::IndexList,
    item_index::Int=grid_cell.data[grid_cell.index]
)
    offset = grid_cell.items_offset
    if item_index == 0
        return nothing
    end
    return (item_index, grid_cell.data[item_index + offset])
end

Base.IteratorEltype(::Type{<:IndexList}) = Base.HasEltype()

Base.eltype(::Type{IndexList{T}}) where {T} = T

Base.IteratorSize(::Type{<:IndexList}) = Base.SizeUnknown()
