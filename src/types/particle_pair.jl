struct ParticlePair{T}
    ind1::Int
    ind2::Int
    r12::SVector{3,T}
    sqdist::T
end

"""
    ParticlePair

Data structure to hold information about a pair of particles. Use
    [`atom_indices(p)`](@ref atom_indices) to get the indices of particles in the pair,
    [`pbcvector(p)`](@ref pbcvector) to get the vector between particles regarding the
    periodic boundary conditions, [`pbcdist2(p)`](@ref pbcdist2) and
    [`pbcdist(p)`](@ref pbcdist) for the square of `pbcvector(p)`'s magnitude and its
    magnitude, respectively.

    See also: [`atom_indices`](@ref), [`pbcvector`](@ref), [`pbcdist`](@ref),
    [`pbcdist2`](@ref)
"""
function ParticlePair end

function ParticlePair(p1::MDParticle{MDState{T}}, p2::MDParticle{MDState{T}}) where {T}
    if p1.state !== p2.state
        throw(ArgumentError("Both particles must belong to the same system"))
    end
    ind1, ind2 = p1.idx, p2.idx
    parent_state = p1.state
    coord = parent_state.coord
    @boundscheck if !(ind1 in eachindex(coord) && ind2 in eachindex(coord))
        throw(ArgumentError("Particle indices must be in the valid range"))
    end
    boxsize = diag(boxvectors(parent_state))
    r12 = pbcvector(coord[ind1], coord[ind2], boxsize, parent_state.pbc)
    return ParticlePair{T}(ind1, ind2, r12, mag2(r12))
end

Base.@propagate_inbounds function ParticlePair(
    state::MDState{T}, ind1::Integer, ind2::Integer,
) where {T}
    coord = state.coord
    @boundscheck if !(ind1 in eachindex(coord) && ind2 in eachindex(coord))
        throw(ArgumentError("Particle indices must be in the valid range"))
    end
    boxsize = diag(boxvectors(state))
    r12 = pbcvector(coord[ind1], coord[ind2], boxsize, state.pbc)
    return ParticlePair{T}(ind1, ind2, r12, mag2(r12))
end

"""
    atom_indices(pair::ParticlePair)

Return the indices of atoms in `pair`.
"""
atom_indices(pair::ParticlePair) = (pair.ind1, pair.ind2)
