"""
    neighbor_pairs(system::MDState; rcut::Real, exclude=nothing)

Return an iterable objects of all pairs in `system` separated by the distance less than
    `rcut`, accounting for periodic boundaries. If `exclude(i, j) == true`, then the pair
    `(i, j)` is excluded from iteration.
"""
function neighbor_pairs(system::MDState; rcut::Real, exclude=nothing)
    return NeighborPairs(system, rcut, exclude)
end

function neighbor_pairs(system::MDState, rcut::Real; exclude=nothing)
    return NeighborPairs(system, rcut, exclude)
end

struct NeighborPairs{X,B,C,I<:CartesianIndices,T,F}
    coord::X
    boxsize::B
    cell_list::C
    cartinds::I
    rsq::T
    pbc::NTuple{3,Bool}
    exclude::F
end

Base.IteratorSize(::Type{<:NeighborPairs}) = Base.SizeUnknown()

Base.IteratorEltype(::Type{<:NeighborPairs}) = Base.HasEltype()

Base.eltype(::Type{<:NeighborPairs{VecVec{T}}}) where {T} = ParticlePair{T}

function NeighborPairs(system::MDState, rcut::Real, exclude=nothing)
    cell_list = CellList(system, rcut)
    return NeighborPairs(
        system.coord,
        diag(boxvectors(system)),
        cell_list,
        CartesianIndices(cell_list),
        rcut^2,
        system.pbc,
        exclude,
    )
end

Base.@inline function Base.iterate(pairs::NeighborPairs)
    cartinds_next = iterate(pairs.cartinds)
    while cartinds_next !== nothing
        idx = cartinds_next[1]
        cartinds_iterstate = cartinds_next[2]
        @inbounds cell_particles = pairs.cell_list[idx]
        if !isempty(cell_particles)
            neigh_i = neigh_atoms(pairs.cell_list, idx)
            neigh_i_next = iterate(neigh_i)
            while neigh_i_next !== nothing
                ind1 = neigh_i_next[1]
                neigh_iterstate = neigh_i_next[2]
                @inbounds r1 = pairs.coord[ind1]
                cell_next = iterate(cell_particles)
                while cell_next !== nothing
                    ind2 = cell_next[1]
                    cell_iterstate = cell_next[2]
                    ind2 < ind1 || break
                    @inbounds r2 = pairs.coord[ind2]
                    if pairs.exclude === nothing || !pairs.exclude(ind1, ind2)
                        r12 = pbcvector(r1, r2, pairs.boxsize, pairs.pbc)
                        sqdist = mag2(r12)
                        if sqdist < pairs.rsq
                            return (
                                ParticlePair(ind1, ind2, r12, sqdist),
                                (
                                    cartinds_iterstate,
                                    neigh_i,
                                    neigh_iterstate,
                                    ind1,
                                    r1,
                                    cell_particles,
                                    cell_iterstate,
                                ),
                            )
                        end
                    end
                    cell_next = iterate(cell_particles, cell_iterstate)
                end
                neigh_i_next = iterate(neigh_i, neigh_iterstate)
            end
        end
        cartinds_next = iterate(pairs.cartinds, cartinds_iterstate)
    end
    return nothing
end

Base.@inline function Base.iterate(
    pairs::NeighborPairs,
    state
)
    local cartinds_next
    (
        cartinds_iterstate,
        neigh_i,
        neigh_iterstate,
        ind1,
        r1,
        cell_particles,
        cell_iterstate,
    ) = state

    @goto iter_continue
    while cartinds_next !== nothing
        idx = cartinds_next[1]
        cartinds_iterstate = cartinds_next[2]
        @inbounds cell_particles = pairs.cell_list[idx]
        if !isempty(cell_particles)
            neigh_i = neigh_atoms(pairs.cell_list, idx)
            neigh_i_next = iterate(neigh_i)
            while neigh_i_next !== nothing
                ind1 = neigh_i_next[1]
                neigh_iterstate = neigh_i_next[2]
                @inbounds r1 = pairs.coord[ind1]
                cell_next = iterate(cell_particles)
                while cell_next !== nothing
                    ind2 = cell_next[1]
                    cell_iterstate = cell_next[2]
                    ind2 < ind1 || break
                    @inbounds r2 = pairs.coord[ind2]
                    if pairs.exclude === nothing || !pairs.exclude(ind1, ind2)
                        r12 = pbcvector(r1, r2, pairs.boxsize, pairs.pbc)
                        sqdist = mag2(r12)
                        if sqdist < pairs.rsq
                            return (
                                ParticlePair(ind1, ind2, r12, sqdist),
                                (
                                    cartinds_iterstate,
                                    neigh_i,
                                    neigh_iterstate,
                                    ind1,
                                    r1,
                                    cell_particles,
                                    cell_iterstate,
                                ),
                            )
                        end
                    end
                    @label iter_continue
                    cell_next = iterate(cell_particles, cell_iterstate)
                end
                neigh_i_next = iterate(neigh_i, neigh_iterstate)
            end
        end
        cartinds_next = iterate(pairs.cartinds, cartinds_iterstate)
    end
    return nothing
end

function Base.foreach(fn, pairs::NeighborPairs)
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                pairs.exclude !== nothing && pairs.exclude(i, j) && continue
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    fn(ParticlePair(i, j, r12, sqdist))
                end
            end
        end
    end
    return nothing
end

function Base.foldl(op, pairs::NeighborPairs; init)
    result = init
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                pairs.exclude !== nothing && pairs.exclude(i, j) && continue
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    result = op(result, ParticlePair(i, j, r12, sqdist))
                end
            end
        end
    end
    return result
end

Base.reduce(op, pairs::NeighborPairs; init) = foldl(op, pairs; init)

function Base.mapfoldl(fn, op, pairs::NeighborPairs; init)
    result = init
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                pairs.exclude !== nothing && pairs.exclude(i, j) && continue
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    result = op(result, fn(ParticlePair(i, j, r12, sqdist)))
                end
            end
        end
    end
    return result
end

Base.mapfoldl(fn, ::typeof(+), pairs::NeighborPairs; kw...) = sum(fn, pairs; kw...)

Base.mapfoldl(fn, ::typeof(*), pairs::NeighborPairs; kw...) = prod(fn, pairs; kw...)

Base.mapreduce(fn, op, pairs::NeighborPairs; kw...) = mapfoldl(fn, op, pairs; kw...)

function Base.sum(
    fn, pairs::NeighborPairs{VecVec{T}};
    init=zero(fn(ParticlePair(1, 1, zero(SVector{3,T}), zero(T))))
) where {T}
    s = init
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                pairs.exclude !== nothing && pairs.exclude(i, j) && continue
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    s += fn(ParticlePair(i, j, r12, sqdist))
                end
            end
        end
    end
    return s
end

function Base.prod(
    fn, pairs::NeighborPairs{VecVec{T}};
    init=one(fn(ParticlePair(1, 1, zero(SVector{3,T}), zero(T))))
) where {T}
    p = init
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                pairs.exclude !== nothing && pairs.exclude(i, j) && continue
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    p *= fn(ParticlePair(i, j, r12, sqdist))
                end
            end
        end
    end
    return p
end

function Base.count(pred, pairs::NeighborPairs; init=0)
    num = init
    rsq = pairs.rsq
    cell_list = pairs.cell_list
    boxsize = pairs.boxsize
    coord = pairs.coord
    for idx in CartesianIndices(cell_list)
        cell_particles = cell_list[idx]
        isempty(cell_particles) && continue
        neigh_i = neigh_atoms(cell_list, idx)
        for i in neigh_i
            r1 = coord[i]
            for j in cell_particles
                j < i || break
                pairs.exclude !== nothing && pairs.exclude(i, j) && continue
                r2 = coord[j]
                r12 = pbcvector(r1, r2, boxsize, pairs.pbc)
                sqdist = mag2(r12)
                if sqdist < rsq
                    num += pred(ParticlePair(i, j, r12, sqdist))
                end
            end
        end
    end
    return num
end

function Base.in(pair::ParticlePair, neigh_pairs::NeighborPairs)
    coords = neigh_pairs.coords
    i, j = atom_indices(pair)
    r1, r2 = coords[i], coords[j]
    if pbcvector(r1, r2, neigh_pairs.boxsize, neigh_pairs.pbc) == pbcvector(pairs)
        return pbcdist2(pair) < neigh_pairs.rsq
    else
        return false
    end
end
