struct Molecule{C,S<:MDState}
    state::S
    id::Int
    natoms::Int
    mass::Float64
    coord::SVector{3,Float64}
    vel::SVector{3,Float64}
    image::SVector{3,Int16}
    atom_inds::IndexList{Int}
end

struct Molecules{C,S<:MDState}<:AbstractVector{Molecule{C,S}}
    parent::S
    id::Vector{Int}
    list::IndexListArray{1,Int}

    function Molecules{:mass,S}(parent, id, list) where {S<:MDState}
        if !hasmass(parent)
            throw(ArgumentError("Cannot use center of mass without specified masses"))
        end
        return new{:mass,S}(parent, id, list)
    end

    function Molecules{:geometry,S}(parent, id, list) where {S<:MDState}
        return new{:geometry,S}(parent, id, list)
    end

    function Molecules{C,S}(parent, id, list) where {S<:MDState,C}
        throw(ArgumentError("Unknown center style $C, available center styles are `:geometry` and `:mass`"))
    end
end

Base.@propagate_inbounds function Molecules{C}(system::S) where {C,S<:MDState}
    mol_ids = sort!(unique(system.mol))
    nmol = length(mol_ids)
    list = IndexListArray(; dims=(nmol,), nitems=natoms(system))
    for k in reverse(eachindex(system.mol))
        molid = system.mol[k]
        imol = searchsortedfirst(mol_ids, molid)
        push!(list[begin+imol-1], k) # cell list is zero-based
    end
    return Molecules{C,S}(system, mol_ids, list)
end

function Base.show(io::IO, mol::Molecules)
    outfmt = Format("""
    List of molecules (total %d)""")
    format(
        io,
        outfmt,
        length(mol),
    )
end

function Base.length(mol::Molecules)
    return size(mol.list)[1]
end

function Base.size(mol::Molecules)
    return size(mol.list)
end

Base.@propagate_inbounds function Base.getindex(
    mol::Molecules{:mass,S}, i::Integer
) where {S<:MDState}
    ind = i - one(i) # account for zero-based list indexing
    @boundscheck i in eachindex(mol.id) || BoundsError(mol, i)
    state = mol.parent
    atomcoord = unwrapped_positions(state)
    atomvel = state.vel
    coord = state.origin
    vel = SVector(0.0, 0.0, 0.0)
    mass = 0.0
    natoms = 0
    boxsize = diag(boxvectors(state))
    atommass = masses(state)
    for iatom in mol.list[begin+ind]
        natoms += 1
        amass = atommass[iatom]
        mass += amass
        if natoms == 1
            coord = atomcoord[iatom]
        else
            atom_disp = pbcvector(coord, atomcoord[iatom], boxsize, state.pbc)
            coord += amass / mass * atom_disp
        end
        vel += atomvel[iatom] * amass
    end
    vel /= mass

    offset_coord = coord - state.origin
    image = floor.(Int16, offset_coord ./ boxsize)
    coord -= boxvectors(state) * image
    atom_list = mol.list[ind]
    return Molecule{:mass,S}(state, mol.id[i], natoms, mass, coord, vel, image, atom_list)
end

Base.@propagate_inbounds function Base.getindex(
    mol::Molecules{:geometry,S}, i::Integer
) where {S<:MDState}
    ind = i - one(i) # account for zero-based list indexing
    @boundscheck i in eachindex(mol.id) || BoundsError(mol, i)
    state = mol.parent
    atomcoord = unwrapped_positions(state)
    atomvel = state.vel
    has_mass = hasmass(state)
    coord = state.origin
    vel = SVector(0.0, 0.0, 0.0)
    mass = has_mass ? 0.0 : NaN
    natoms = 0
    boxsize = diag(boxvectors(state))
    amass = masses(state)
    for iatom in mol.list[begin+ind]
        natoms += 1
        if has_mass
            mass += amass[iatom]
        end
        if natoms == 1 # first atom
            coord = atomcoord[iatom]
        else
            atom_disp = pbcvector(coord, atomcoord[iatom], boxsize, state.pbc)
            coord += atom_disp / natoms
        end
        vel += atomvel[iatom]
    end
    vel /= natoms

    offset_coord = coord - state.origin
    image = floor.(Int16, offset_coord ./ boxsize)
    coord -= boxvectors(state) * image
    atom_list = mol.list[ind]
    return Molecule{:geometry,S}(state, mol.id[i], natoms, mass, coord, vel, image, atom_list)
end

Base.@propagate_inbounds function Base.iterate(mol::Molecules, ind::Int=1)
    if ind in eachindex(mol.id)
        return mol[ind], ind + 1
    else
        return nothing
    end
end

Base.IteratorEltype(::Type{<:Molecules}) = Base.HasEltype()

Base.eltype(::Type{Molecules{C,S}}) where {S,C} = Molecule{C,S}

Base.IteratorSize(::Type{<:Molecules}) = Base.HasLength()

function Base.show(io::IO, mol::Molecule)
    outfmt = Format("""
    Molecule #%d""")
    format(io, outfmt, mol.id)
    #join(io, mol.atoms, ' ')
end

Base.@propagate_inbounds function Base.iterate(mol::Molecule)
    atoms = mol.atom_inds
    next = iterate(atoms)
    return next
end

Base.@propagate_inbounds function Base.iterate(mol::Molecule, state)
    atoms = mol.atom_inds
    next = iterate(atoms, state)
    return next
end

Base.IteratorEltype(::Type{<:Molecule}) = Base.HasEltype()

Base.eltype(::Type{<:Molecule}) = Int

Base.IteratorSize(::Type{<:Molecule}) = Base.HasLength()

Base.length(mol::Molecule) = mol.natoms

"""
    molecules(system::MDState; centering::Symbol=:geometry)

Return an array-like object for molecules in `system`. `centering` specifies if molecular
    centers are geometric centers (`centering = :geometry`) or centers of mass
    (`centering = :mass`). If centers are geometric, the position of center is the
    arithmetic average of the positions of atoms in molecule, and the velocity of molecule
    is computed as the arithmetic average of atom velocities. If centers are centers of
    mass, the COM positions and velocities of molecules are computed.
"""
function molecules(system::S; centering::Symbol=:geometry) where {S<:MDState}
    if !hasmass(system) && centering === :mass
        @warn "Cannot use center of mass without specified masses, using center of geometry for molecules"
        centering = :geometry
    end
    return Molecules{centering}(system)
end

function wrapped_position(mol::Molecule)
    return mol.coord
end

function unwrapped_position(mol::Molecule)
    return mol.coord + boxvectors(mol.state) * mol.image
end

"""
    map(fn, mol::Molecules)

Apply `fn` to each of the molecules in `mol`. The return value must be a `NamedTuple` of
    the form `(property1=val1, property2=val2, ...)`. The output of the mapping is `MDState`
    with the same box as `mol`'s parent state, where the particles are molecules in `mol`,
    the positions and velocities correspond to the center-of-mass parameters for the
    molecules, and `property1`, `property2` etc. are added to the list of additional
    properties.
"""
function Base.map(fn, mol::Molecules{C,S}) where {C,S<:MDState}
    state = mol.parent
    buf = S(; box_vectors=boxvectors(state), origin=state.origin, pbc=state.pbc)

    molcoord = buf.coord
    molvel = buf.vel
    update_coord!(buf, Iterators.repeated(SVector(0.0, 0.0, 0.0), length(mol)))
    has_mass = hasmass(state)
    if has_mass
        addproperty!(buf; name=:mass, data=range(0.0, stop=0.0, length=length(mol)))
        molmass = masses(buf)
    end
    for (idx, m) in enumerate(mol)
        molcoord[idx] = m.coord
        molvel[idx] = m.vel
        buf.id[idx] = m.id
        buf.mol[idx] = m.id
        buf.image[idx] = m.image
        if has_mass
            molmass[idx] = m.mass
        end
    end

    mapping = [fn(m) for m in mol]
    for key in keys(first(mapping))
        addproperty!(
            buf
            ;
            name=key, data=[getindex(item, key) for item in mapping], allow_sharing=true
        )
    end
    return buf
end
