"""
    read_dump!(file::Union{IO,AbstractString}, system::MDState; mode=:update)

Read state from `file` into `system`. With `mode=:update` (default), the fields that are
    available in the file are updated in `system`. With `mode=:replace`, `system` is fully
    cleared and the state is read afresh.
"""
function read_dump!(file::IO, system::MDState{T}; mode=:update) where {T}
    mode in (:replace, :update) || error(
        "Invalid reading mode. Valid modes are `:update` and `:replace`."
    )
    if mode === :replace
        clear_state!(system)
    end
    a1 = readline(file)
    a1 == "ITEM: TIMESTEP" || error(
        "Invalid dump format: expected ITEM: TIMESTEP on line 1."
    )
    a = readline(file)
    t = tryparse(Int, a)
    t === nothing && error("Invalid dump format: expected timestep on line 2.")

    a1 = readline(file)
    a1 == "ITEM: NUMBER OF ATOMS" || error(
        "Invalid dump format: expected ITEM: NUMBER OF ATOMS on line 3."
    )
    a = readline(file)
    n = tryparse(Int, a)
    n === nothing && error("Invalid dump format: expected number of atoms on line 4.")

    if mode === :update && n != natoms(system)
        error("Cannot update the state: number of atoms does not match. Use `mode=:replace` keyword to clear the initial state.")
    elseif mode === :replace
        system.nreal = n
    end

    a = readline(file)
    if (startswith(a, "ITEM: BOX BOUNDS"))
        atok = split(a)
        pbctype = (atok[end-2], atok[end-1], atok[end]) .== "pp"

        triclinic_box = length(atok) == 9
        ortho_box = length(atok) == 6

        ortho_box || triclinic_box ||
            error("Invalid dump format: cannot determine the box type.")
        s1 = MVector{3,T}(0, 0, 0)
        s2 = MVector{3,T}(0, 0, 0)
        tilt = MVector{3,T}(0, 0, 0)
        for i in 1:3
            a = readline(file)
            c = split(a)
            if triclinic_box
                s1[i], s2[i], tilt[i] = (tryparse(T, tok) for tok in c)
            else
                s1[i], s2[i] = (tryparse(T, tok) for tok in c)
            end
        end

        s1 .-= (
            min(0, tilt[1], tilt[2], tilt[1] + tilt[2]), min(0, tilt[3]), zero(T)
        )
        s2 .-= (
            max(0, tilt[1], tilt[2], tilt[1] + tilt[2]), max(0, tilt[3]), zero(T)
        )
        system.origin = s1
        system.box_vectors = SMatrix{3,3,T}(
            s2[1] - s1[1], 0, 0, tilt[1], s2[2] - s1[2], 0, tilt[2], tilt[3], s2[3] - s1[3]
        )
    else
        error("Invalid dump format: expected ITEM: BOX BOUNDS on line 5.")
    end
    a = readline(file)
    colnames = split(a)[3:end]

    if !isdiag(system.box_vectors)
        coordcols = intersect(
            colnames,
            ("x", "y", "z", "xs", "ys", "zs", "xu", "yu", "zu", "xsu", "ysu", "zsu"),
        )
        if any(contains('s'), coordcols) && any(!contains('s'), coordcols)
            error("Cannot mix scaled and unscaled coordinated with a triclinic box.")
        end
    end
    table = (; map(s -> table_col(s, T), colnames)...)
    __fill_table!(file, table, n)

    __copytable!(system, table; mode=mode)
    system.pbc = pbctype
    return system
end

can_read_state(file) = !eof(file)

function __fill_table!(file::IO, table::NamedTuple, n::Integer)
    for _ in 1:n
        if eof(file)
            @warn "EOF while reading the dump file! Expect corrupted data"
            break
        end
        a = readline(file)
        tokens = split(a)
        if length(tokens) != length(table)
            @warn "EOF while reading the dump file! Expect corrupted data"
            break
        end
        parse_row!(table, tokens)
    end
end

function read_dump!(filepath::AbstractString, system::MDState; mode::Symbol=:update)
    open(filepath) do io
        read_dump!(io, system; mode=mode)
    end
end

"""
    read_dump(file::Union{IO,AbstractString})

Return an `MDState` read from a LAMMPS dump `file`.
"""
function read_dump(input::Union{IO,AbstractString})
    state = MDState()
    read_dump!(input, state; mode=:replace)
    return state
end

function __copytable!(system::MDState{T}, table::NamedTuple; mode::Symbol) where T
    n = length(first(table))

    # mode is one of (:update, :replace)
    # with mode = :update, we assume that the length of the table is already equal to the
    # number of atoms in the system

    coord, vel = system.coord, system.vel
    if mode === :replace
        resize!(system.image, n)
        resize!(system.coord, n)
        resize!(system.vel, n)
        resize!(system.id, n)
        resize!(system.type, n)
        resize!(system.mol, n)
    end

    if hasproperty(table, :id)
        sortperm!(system.id, table.id)
    else
        system.id .= 1:n
    end

    # By default:
    # - each particle is a separate molecule
    # - all particles are of type 1
    if !hasproperty(table, :mol)
        system.mol .= 1:n
    end

    if !hasproperty(table, :type)
        system.type .= 1
    end

    xcolumn, ycolumn, zcolumn = nothing, nothing, nothing
    ixcolumn, iycolumn, izcolumn = nothing, nothing, nothing
    xxscale, yyscale, zzscale = one(T), one(T), one(T)
    xyscale, xzscale, yzscale = zero(T), zero(T), zero(T)
    x0, y0, z0 = zero(T), zero(T), zero(T)

    keepx, keepy, keepz = true, true, true
    keepix, keepiy, keepiz = true, true, true
    if hasproperty(table, :xs)
        xcolumn = table.xs
        xxscale = system.box_vectors[1, 1]
        x0 = system.origin[1]
        keepx = false
    end
    if hasproperty(table, :x)
        xcolumn = table.x
        xxscale = one(T)
        x0 = zero(T)
        keepx = false
    end
    if hasproperty(table, :xsu)
        xcolumn = table.xsu
        xxscale = system.box_vectors[1, 1]
        x0 = system.origin[1]
        keepx = false
        keepix = false
    end
    if hasproperty(table, :xu)
        xcolumn = table.xu
        xxscale = one(T)
        x0 = zero(T)
        keepx = false
        keepix = false
    end
    if hasproperty(table, :ys)
        ycolumn = table.ys
        yyscale = system.box_vectors[2, 2]
        xyscale = system.box_vectors[1, 2]
        y0 = system.origin[2]
        keepy = false
    end
    if hasproperty(table, :y)
        ycolumn = table.y
        yyscale = one(T)
        xyscale = zero(T)
        y0 = zero(T)
        keepy = false
    end
    if hasproperty(table, :ysu)
        ycolumn = table.ysu
        yyscale = system.box_vectors[2, 2]
        xyscale = system.box_vectors[1, 2]
        y0 = system.origin[2]
        keepy = false
        keepiy = false
    end
    if hasproperty(table, :yu)
        ycolumn = table.yu
        yyscale = one(T)
        xyscale = zero(T)
        y0 = zero(T)
        keepy = false
        keepiy = false
    end
    if hasproperty(table, :zs)
        zcolumn = table.zs
        zzscale = system.box_vectors[3, 3]
        xzscale = system.box_vectors[1, 3]
        yzscale = system.box_vectors[2, 3]
        z0 = system.origin[3]
        keepz = false
    end
    if hasproperty(table, :z)
        zcolumn = table.z
        zzscale = one(T)
        xzscale = zero(T)
        yzscale = zero(T)
        z0 = zero(T)
        keepz = false
    end
    if hasproperty(table, :zsu)
        zcolumn = table.zsu
        zzscale = system.box_vectors[3, 3]
        xzscale = system.box_vectors[1, 3]
        yzscale = system.box_vectors[2, 3]
        z0 = system.origin[3]
        keepz = false
        keepiz = false
    end
    if hasproperty(table, :zu)
        zcolumn = table.zu
        zzscale = one(T)
        xzscale = zero(T)
        yzscale = zero(T)
        z0 = zero(T)
        keepz = false
        keepiz = false
    end
    if hasproperty(table, :ix) && !keepix
        ixcolumn = table.ix
    end
    if hasproperty(table, :iy) && !keepiy
        iycolumn = table.iy
    end
    if hasproperty(table, :iz) && !keepiz
        izcolumn = table.iz
    end

    if mode === :replace
        fill!(system.image, Int16.((0, 0, 0)))
        fill!(coord, (x0, y0, z0))
        fill!(vel, zero(eltype(vel)))
    end

    vecf = reinterpret(T, coord)

    xvecf = @view vecf[1:3:end]
    yvecf = @view vecf[2:3:end]
    zvecf = @view vecf[3:3:end]

    ivecf = reinterpret(Int16, system.image)
    ixvecf = @view ivecf[1:3:end]
    iyvecf = @view ivecf[2:3:end]
    izvecf = @view ivecf[3:3:end]

    if mode === :update
        keep_coord = SVector{3,Bool}(keepx, keepy, keepz)
        if !all(keep_coord)
            for i in eachindex(coord)
                coord[i] = coord[i] .* keep_coord .+ (x0, y0, z0)
            end
        end
    end

    vecfv = reinterpret(T, vel)
    xvecfv = @view vecfv[1:3:end]
    yvecfv = @view vecfv[2:3:end]
    zvecfv = @view vecfv[3:3:end]

    for (name, col) in pairs(table)
        if name === :vx
            xvecfv .= @view col[system.id]
        elseif name === :vy
            yvecfv .= @view col[system.id]
        elseif name === :vz
            zvecfv .= @view col[system.id]
        elseif name === :mol
            system.mol .= @view col[system.id]
        elseif name === :type
            system.type .= @view col[system.id]
        elseif col === ixcolumn
            ixvecf .= @view(col[system.id])
        elseif col === iycolumn
            iyvecf .= @view(col[system.id])
        elseif col === izcolumn
            izvecf .= @view(col[system.id])
        # WARNING: assuming an orthogonal cell
        elseif col === xcolumn
            ixvecf .+= fld.(@view(col[system.id]) .* xxscale .- x0, system.box_vectors[1, 1])
            xvecf .+= mod.(@view(col[system.id]) .* xxscale .- x0, system.box_vectors[1, 1])
        elseif col === ycolumn
            iyvecf .+= fld.(@view(col[system.id]) .* yyscale .- y0, system.box_vectors[2, 2])
            #xvecf .+= @view(col[system.id]) .* xyscale
            yvecf .+= mod.(@view(col[system.id]) .* yyscale .- y0, system.box_vectors[2, 2])
        elseif col === zcolumn
            izvecf .+= fld.(@view(col[system.id]) .* zzscale .- z0, system.box_vectors[3, 3])
            #xvecf .+= @view(col[system.id]) .* xzscale
            #yvecf .+= @view(col[system.id]) .* yzscale
            zvecf .+= mod.(@view(col[system.id]) .* zzscale .- z0, system.box_vectors[3, 3])
        elseif name !== :id
            if mode === :replace
                addproperty!(system; name, data=@view(col[system.id]))
            elseif mode === :update
                system._scalars[name] .= @view(col[system.id])
            end
        end
    end

    if hasproperty(table, :id)
        system.id .= table.id
        sort!(system.id)
    end
    return system
end

"""
    state_is_valid(system)

Verify that the length of coordinates, velocities and forces arrays are the same, that the
    size array is defined and that sizes along all axes are positive.
"""
function state_is_valid(system)
    length(system.coord) == length(system.vel) || (return false)
    all(x -> x > 0, system.size) || (return false)
    !isempty(system.coord) > 0 || (return false)
end

function table_col(s, T::Type{<:AbstractFloat}=Float64)
    if s in ("id", "mol", "type")
        return Symbol(s) => Int[]
    else
        return Symbol(s) => T[]
    end
end

function parse_row!(table, tokens)
    for (col, token) in zip(table, tokens)
        push!(col, parse(eltype(col), token))
    end
    return
end

function update_coord!(system::MDState{T}, newcoord) where T
    nextra = length(newcoord) - natoms(system)
    if nextra > 0
        append!(system.image, Iterators.repeated(zero(eltype(system.image)), nextra))
        append!(system.vel, Iterators.repeated(zero(eltype(system.vel)), nextra))
        append!(system.type, Iterators.repeated(1, nextra))
        append!(system.id, range(get(system.id, length(system.id), 0)+1, length=nextra))
        append!(system.mol, range(get(system.mol, length(system.mol), 0)+1, length=nextra))
    end
    resize!(system.coord, length(newcoord))
    for (i, coord) in enumerate(newcoord)
        offset_coord = coord - system.origin
        coord = system.origin + mod.(offset_coord, diag(boxvectors(system)))
        image = floor.(Int16, offset_coord ./ diag(boxvectors(system)))
        system.coord[i] = coord
        system.image[i] = image
    end
    system.nreal = length(newcoord)
    return system
end
