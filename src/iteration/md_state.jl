Base.IteratorEltype(::Type{<:MDState}) = Base.HasEltype()

Base.eltype(T::Type{<:MDState}) = MDParticle{T}

Base.IteratorSize(::Type{<:MDState}) = Base.HasLength()

Base.@propagate_inbounds function Base.iterate(state::MDState, idx::Int=1)
    idx in eachindex(state.coord) || return nothing
    return MDParticle(state, idx), idx+1
end

Base.length(state::MDState) = length(state.coord)

"""
    filter(predicate, system::MDState)

Return a copy of `system` which contains only the particles satisfying the `predicate`.
"""
function Base.filter(fn, system::MD) where {MD<:MDState}
    keep = [fn(part) for part in system]
    filtered = MD(box_vectors=boxvectors(system), pbc=system.pbc, origin=system.origin)
    map((:id, :type, :mol, :image, :coord, :vel)) do attr
        @views append!(getfield(filtered, attr), getfield(system, attr)[keep])
    end
    for (k, v) in system._scalars
       @views addproperty!(filtered; name=k, data=v[keep], allow_sharing=true)
    end
    for (k, v) in system._vectors
        @views addproperty!(filtered; name=k, data=v[keep], allow_sharing=true)
    end
    filtered.nreal = length(filtered.coord)
    return filtered
end
