# MDProcessing.jl documentation

## Reading LAMMPS output files

```@docs
read_dump

read_dump!
```

## Type constructors
```@docs
MDState
```

## Basic interface
```@docs
natoms

boxvectors

boxorigin

Base.length(::MDState)

hasmass

masses

wrapped_positions

unwrapped_positions

velocities

id_tags

mol_tags

MDParticle

Base.filter(fn, ::MDState)
```

## Atoms
```@docs
MDProcessing.MDParticle

wrapped_position(p::MDProcessing.MDParticle)

unwrapped_position(p::MDProcessing.MDParticle)

id_tag(p::MDProcessing.MDParticle)

mol_tag(p::MDProcessing.MDParticle)
```

## Molecules

```@docs
molecules

centering

atom_indices

coord_offsets

wrapped_position(mol::MDProcessing.Molecule)

unwrapped_position(mol::MDProcessing.Molecule)

Base.map(fn, mol::MDProcessing.Molecules)
```

### Shape parameters

```@docs
molecule_planes

molecule_lines

gyration_radius

gyration_tensor

inertia_tensor

asphericity

acilindricity

shape_anisotropy

anisotropy_factors
```

## Neighbor search

```@docs
neighbor_pairs

MDProcessing.ParticlePair

atom_indices(p::MDProcessing.ParticlePair)

pbcvector

pbcdist

pbcdist2
```

## Trajectory functions

```@docs
traj_average

traj_correlate
```

## Adding a property

```@docs
addproperty!
```

## Autocorrelation

The functions below are part of `MDProcessing.Autocorr` module.

```@docs
Autocorr.acf_fft

Autocorr.integrate
```

## Other analysis functions

```@docs
vel_distribution

vel_distribution!

msd

rdf

rdf!

rdfw

rdfw!
```
