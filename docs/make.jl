using Documenter, MDProcessing

#DocMeta.setdocmeta!(MDProcessing, :DocTestSetup, :(using MDProcessing); recursive=true)

makedocs(;
    modules = [MDProcessing, MDProcessing.Autocorr],
    checkdocs = :exports,
    sitename = "MDProcessing.jl documentation",
    format = Documenter.HTML(
        prettyurls = get(ENV, "CI", nothing) == "true"
    ),
    repo = "https://gitlab.com/pisarevvv/mdprocessing.jl/blob/{commit}{path}#{line}",
    pages = [
        "Home" => "index.md",
    ],
    warnonly = true,
)

deploydocs(;
  repo = "gitlab.com/pisarevvv/mdprocessing.jl",
  devurl = "dev",
  versions = ["stable" => "v^", "v#.#.#", "dev" => "dev"],
  devbranch = "main",
)
