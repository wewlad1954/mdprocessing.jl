@testset "Trajectory average" begin
    system = read_dump("./data/lj.T1.d0.6.0.dump")

    rdf_single = rdf(system, boxvectors(system)[1,1] / 3, 50)

    # Reading trajectory from file mask
    @test rdf_single == traj_average("./data/lj.T1.d0.6.*.dump") do system
        rdf(system, boxvectors(system)[1,1] / 3, 50)
    end

    # Specifying a single file name for trajectory
    @test rdf_single == traj_average("./data/lj.T1.d0.6.0.dump") do system
        rdf(system, boxvectors(system)[1,1] / 3, 50)
    end

    # Timesteps have leading zeros
    dpe_ave_a = traj_average("./data/1,1-DPE.0*"; timesteps=1000:1000:2000) do system
        rdf(system, 10.0, 50)
    end
    @test dpe_ave_a == traj_average("./data/1,1-DPE.*"; timesteps=1000:1000:2000) do system
        rdf(system, 10.0, 50)
    end

    # Mask does not correspond to a valid file
    @test_throws ArgumentError traj_average(
        "./data/lj.T1.d0.7.*"; timesteps=1000:1000:2000
    ) do system
        rdf(system, 10.0, 50)
    end

    @test_throws ArgumentError traj_average(
        "./data/1,1-DPE.*"; timesteps=10_000:100_000
    ) do system
        rdf(system, 10.0, 50)
    end

    @test begin
        dpe_rdfs = [
            rdf(read_dump("./data/1,1-DPE.$n"), 10.0, 50)
            for n in ["00000", "01000", "02000"]
        ]

        sum(dpe_rdfs) / 3 == traj_average("./data/1,1-DPE.*") do system
            rdf(system, 10.0, 50)
        end
    end

    @test begin
        dpe_sums = [
            sum(r -> dot(r, r), read_dump("./data/1,1-DPE.$n").coord)
            for n in ["00000", "01000", "02000"]
        ]

        sum(dpe_sums) / 3 == traj_average("./data/1,1-DPE.*") do system
            sum(r -> dot(r, r), system.coord)
        end
    end
end

@testset "Trajectory correlation" begin
    @test begin
        system = read_dump("./data/lj.T1.d0.7.0.dump")

        ke_corr = traj_correlate("./data/lj.T1.d0.7.*.dump"; step_delta=0) do ref, system
            vref = ref.vel
            vsys = system.vel
            return dot.(vref, vsys) ./ 2
        end

        isapprox(ke_corr, system.c_ekin; rtol=1e-5)
    end

    # Mask does not correspond to a valid file
    @test_throws ArgumentError traj_correlate(
        "./data/lj.T1.d0.7.*"; step_delta=0
    ) do ref, system
            vref = ref.vel
            vsys = system.vel
            return dot.(vref, vsys) ./ 2
    end

    # No suitable pair
    @test_throws ArgumentError traj_correlate(
        "./data/lj.T1.d0.7.*.dump"; step_delta=1000
    ) do ref, system
        vref = ref.vel
        vsys = system.vel
        return dot.(vref, vsys) ./ 2
    end

    @test begin
        s1 = traj_average("./data/1,1-DPE.*") do system
            sum(r -> dot(r, r), system.coord)
        end

        s2 = traj_correlate("./data/1,1-DPE.*"; step_delta=0) do ref, system
            sum(((r1, r2),)->dot(r1, r2), zip(ref.coord, system.coord))
        end

        isapprox(s1, s2, rtol=1e-10)
    end
end
