@testset "Single state properties" begin
    system = read_dump("./data/lj.T1.d0.6.0.dump")

    @test rdf(system, boxvectors(system)[1,1] / 3, 50) isa Matrix

    @test_throws ArgumentError rdf(system, boxvectors(system)[1,1] / 1.5, 50)

    @test begin
        rdf(system, boxvectors(system)[1,1] / 3, 50) ≈
        rdfw(system, boxvectors(system)[1,1] / 3, 50; weights=[1])
    end
end
