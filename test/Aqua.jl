using Aqua

@testset "Aqua.jl" begin
    Aqua.test_all(
      MDProcessing;
      ambiguities=true,
      unbound_args=true,
      undefined_exports=true,
      project_extras=false,
      stale_deps=true,
      deps_compat=false,
      piracies=true,
    )
  end
