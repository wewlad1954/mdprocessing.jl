@testset "Neighbor search" begin
    system = read_dump("./data/lj.T1.d0.7.0.dump")
    pairs = [
        MDProcessing.ParticlePair(system, i, j)
        for i in 1:natoms(system) for j in 1:i-1
    ]

    for rcut in 1.5:0.5:5.5
        pairs_ref = sort!([atom_indices(p) for p in pairs if pbcdist(p) < rcut])
        pairs_our = sort!(atom_indices.(collect(neighbor_pairs(system, rcut))))
        @test pairs_ref == pairs_our
    end
end
