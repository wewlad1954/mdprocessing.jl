@testset "Reading dump files" begin
    @test read_dump("./data/lj.T1.d0.6.0.dump") isa MDState

    state_fresh = read_dump("./data/lj.T1.d0.6.0.dump")
    state_overwrite = let state = read_dump("./data/lj.T1.d0.7.0.dump")
        read_dump!("./data/lj.T1.d0.6.0.dump", state; mode=:replace)
        state
    end
    @test all(
        state_fresh[p] == state_overwrite[p]
        for p in propertynames(state_fresh)
    )

    @test Set(propertynames(state_fresh)) == Set(propertynames(state_overwrite))

    @test all(
        hasproperty(state_fresh, p)
        for p in (:id, :type, :mol, :coord, :vel, :c_epot, :c_ekin)
    )

    @test all(
        length(state_fresh[p]) == length(state_fresh.coord)
        for p in setdiff(
            propertynames(state_overwrite),
            (:box_vectors, :cell_list, :origin, :pbc),
        )
    )

    @test all(
        length(state_overwrite[p]) == length(state_overwrite.coord)
        for p in setdiff(
            propertynames(state_overwrite),
            (:box_vectors, :cell_list, :origin, :pbc),
        )
    )

    @test natoms(state_fresh) / det(boxvectors(state_fresh)) ≈ 0.6
end

@testset "Nonzero origin" begin

    state_c12 = read_dump("./data/C12H26.clay.100000.dump")
    vecs = boxvectors(state_c12)
    r0 = boxorigin(state_c12)

    @test boxorigin(state_c12) == SVector(
        6.3976646999999998e-02,
        -2.4580729999999999e-02,
        -4.8250000000000000e+01
    )

    @test boxvectors(state_c12) == @SMatrix [
        7.1791976647000013e+01 - 6.3976646999999998e-02 0 0
        0 4.1255419269999997e+01 + 2.4580729999999999e-02 0
        0 0 1.0975000000000000e+02 + 4.8250000000000000e+01
    ]

    @test all(
        zip(
            wrapped_positions(state_c12),
            state_c12.image,
            unwrapped_positions(state_c12)
        )
    ) do (r, img, ru)
        isapprox(r + vecs * SVector(img), ru, atol=1e-10)
    end

    @test all(wrapped_coords(state_c12)) do r
        all(vecs \ (r - r0)) do x
            0 <= x < 1
        end
    end skip=true
end
