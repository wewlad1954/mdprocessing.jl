@testset "Molecule iterators" begin
    system = read_dump("./data/benzene.dump")

    @test length(molecules(system)) == natoms(system) / 12

    benzene_c = filter(p->p.type==1, system)

    mol_planes = molecule_planes(benzene_c)

    @test hasproperty(mol_planes, :normal)

    nrm = mol_planes.normal

    @test begin
        mols = molecules(benzene_c; centering=:mass)
        all(zip(nrm, mols)) do (nrmvec, mol)
            all(coord_offsets(mol)) do r
                abs(dot(r, nrmvec)) < 1e-8 * norm(r)
            end
        end
    end
end
