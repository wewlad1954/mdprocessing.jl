@testset "Reading xyz files" begin
    @test read_xyz("benzene.xyz") isa MDState

    state_fresh = read_xyz("benzene.xyz")
    state_overwrite = let state = read_xyz("benzene.xyz")
        read_xyz!("benzene.xyz", state; mode=:replace)
        state
    end
    @test all(
        state_fresh[p] == state_overwrite[p]
        for p in propertynames(state_fresh)
    )

    @test Set(propertynames(state_fresh)) == Set(propertynames(state_overwrite))

    @test all(
        hasproperty(state_fresh, p)
        for p in (:id, :type, :coord)
    )

    @test all(
        length(state_fresh[p]) == length(state_fresh.coord)
        for p in setdiff(
            propertynames(state_overwrite),
            (:box_vectors, :cell_list, :origin, :pbc),
        )
    )

    @test all(
        length(state_overwrite[p]) == length(state_overwrite.coord)
        for p in setdiff(
            propertynames(state_overwrite),
            (:box_vectors, :cell_list, :origin, :pbc),
        )
    )

    # @test natoms(state_fresh) / det(boxvectors(state_fresh)) ≈ 0.6 (?)
end
