@testset "Collection interface" begin
    clay_pentane = read_dump("./data/C5H12.clay.0.dump")

    @test filter(p -> p.type > 13, clay_pentane) isa MDState

    @test begin
        pentane = filter(p -> p.type > 13, clay_pentane)
        rem(natoms(pentane), 17) == 0
    end

    @test begin
        pentane = filter(p -> p.type > 13, clay_pentane)
        sum(unwrapped_positions(pentane)) isa AbstractVector
    end
end
