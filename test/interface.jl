@testset "Interface functions" begin
    state = read_dump("./data/lj.T1.d0.6.0.dump")
    @test natoms(state) == 1000

    # adding a scalar property
    scalar_data = ones(Float64, natoms(state))
    @test addproperty!(state; name="scalar_shared", data=scalar_data, allow_sharing=true) isa Any
    @test state.scalar_shared === scalar_data
    @test addproperty!(state; name="scalar_copy", data=scalar_data, allow_sharing=false) isa Any
    @test state.scalar_copy !== scalar_data

    # adding a vector property
    vector_data = [zeros(3) for _ in state.coord]
    matrix_data = zeros(3, natoms(state))
    @test addproperty!(state; name="vec_from_vec", data=vector_data) isa Any
    @test addproperty!(state; name="vec_from_matr", data=matrix_data) isa Any


    bad_scalars = ones(1)
    bad_matrix = zeros(4, natoms(state))
    # adding a property that is already present must fail
    @test_throws ArgumentError addproperty!(state; name="coord", data=scalar_data) isa Any
    # adding a property with wrong dimensions must fail
    @test_throws DimensionMismatch addproperty!(state; name="bad_scalars", data=bad_scalars) isa Any
    @test_throws DimensionMismatch addproperty!(state; name="bad_vectors", data=bad_matrix) isa Any


    copy_state = deepcopy(state)

    @test all(
        state[p] == copy_state[p]
        for p in propertynames(state)
    )

    @test Set(propertynames(copy_state)) == Set(propertynames(state))

    clear_state!(copy_state)
    @test all(
        p -> isempty(copy_state[p]),
        setdiff(propertynames(copy_state), (:box_vectors, :cell_list, :origin, :pbc))
    )
end
